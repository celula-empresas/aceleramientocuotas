import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;

public class Aceleramiento {
	private static final Logger log = Logger.getLogger( Aceleramiento.class.getName() );
	public static void main(String [] args) {
		
		buscarRut("76042014-K");
		
		//buscarLinea("");
	}
	
	   public static String buscarRut(String rut) {
		   String cantidadLineas = "";
	      try {
			OracleDataSource ds = new OracleDataSource();
			ds.setURL("jdbc:oracle:thin:@10.181.23.143:1535:ivrin");
			System.out.println(ds.getURL());
			Connection conn;
			conn=ds.getConnection("reports","reports");
			System.out.println("connected ");
			Statement statement = conn.createStatement();
		    String queryString = "SELECT COD_CLIENTE FROM GE_CLIENTES WHERE NUM_IDENT = '"+rut+"'";
		    String queryString2 = "";
		    ResultSet rs = statement.executeQuery(queryString);
		    StringBuilder sb = new StringBuilder();
	        if(null != rs) {
	        	int i = 0;
	        	while (rs.next()) {
	        		
	        		if (i!=0) {
	        			sb.append(", ");
	        		}
	        		if (rs.getString(1).equalsIgnoreCase("") || rs.getString(1).equalsIgnoreCase(null) || rs.getString(1).equalsIgnoreCase("_") ) {
		            }
		            else {
		            	sb.append(rs.getString(1));
		            }
	        		i++;
		         }
            	queryString2 = "SELECT num_celular \r\n" +
            			"FROM ga_abocel\r\n" + 
            			"WHERE cod_cliente IN  ("+sb.toString()+")\r\n" + 
            			"AND (cod_causabaja NOT IN ('39') OR cod_causabaja IS NULL) \r\n" + 
            			"AND (cod_situacion  IN ('AAA', 'SAA') or (fec_baja  >= '25/05/2018' AND IND_PORTADO <> 0))";
            	ResultSet rs2 = statement.executeQuery(queryString2);
            	if(null != rs2) {
            		while (rs2.next()) {
            			System.out.println("linea "+rs2.getString(1));
            			cantidadLineas = rs2.getString(1);
            		
            		}
            	}
	        	System.out.println("Termin� de consultar");
		   }
	       rs.close();
	       statement.close();
	       conn.close();
		       
		} catch (SQLException e) {
			
			System.out.println("Error en la consulta a la base de datos "+e);
		}
	      
		return cantidadLineas;
	   }
	   
	   
	   public static String buscarLinea(String rut) {
		   String costoSalida = "";
		   try {
				OracleDataSource ds = new OracleDataSource();
				ds.setURL("jdbc:oracle:thin:@10.181.23.143:1535:ivrin");
				System.out.println(ds.getURL());
				Connection conn;
				conn=ds.getConnection("reports","reports");
				System.out.println("connected ");
				Statement statement = conn.createStatement();
				String queryString = "SELECT" +
						"                                DISTINCT" +
						"                                    (A.NUM_ABONADO)," +
						"                                    A.NUM_CELULAR," +
						"                                    A.COD_CLIENTE," +
						"                                    A.COD_CICLO," +
						"                                    A.FEC_BAJA," +
						"		                            B.FEC_ALTA," +
						"		                            B.cod_causa," +
						"		                            B.cod_articulo," +
						"                                    F.NUM_IDENT," +
						"                                    F.NOM_CLIENTE," +
						"                                    F.NOM_APECLIEN1," +
						"                                    F.NOM_APECLIEN2," +
						"                                    A.COD_SITUACION," +
						"                                    A.COD_PLANTARIF," +
						"                                    A.IND_PORTADO," +
						"                                    E.DES_TIPCONTRATO," +
						"                                    TO_CHAR(C.FEC_ALTA,'DD-MM-YYYY') AS FEC_ALTA," +
						"                                    TO_CHAR(C.FEC_ULT_MOV,'DD-MM-YYYY') AS FEC_ULT_MOV," +
						"                                    TO_CHAR(B.FEC_ALTA,'DD-MM-YYYY') AS FEC_RECAMBIO," +
						"                                    TO_CHAR(A.FEC_BAJA,'DD-MM-YYYY') AS FECHA_BAJA," +
						"                                    H.DES_FABRICANTE," +
						"                                    G.DES_ARTICULO," +
						"                                    A.NUM_SERIE," +
						"                                    A.NUM_IMEI," +
						"                                    CAST(C.PRECIO_FULL AS INT)," +
						"                                    CAST(C.PRECIO_PROM AS INT)," +
						"                                    I.VAL_DTO as VALOR_DESCUENTO," +
						"                                    I.TIP_DTO as TIPO_DESCUENTO," +
						"                                    I.COD_CONCEPTO," +
						"                                    J.COD_CONCEPTO_DTO AS CONCEPTO_FACT_AGRUP," +
						"                                    J.VAL_DTO AS DTO_FACT_AGRUP," +
						"                                    J.TIP_DTO AS TIP_DTO_FACT_AGRUP," +
						"                                    CAST (C.VAL_CUOTA AS INT) AS VALOR_CUOTA," +
						"                                    (C.PRECIO_PROM-I.VAL_DTO) AS CUOTA_INI_ARRIENDO," +
						"                                    CAST (C.VAL_CUOTA AS INT)AS ACELERAMIENTO" +
						"                            FROM" +
						"                                GA_ABOCEL A" +
						"                                LEFT JOIN GA_EQUIPABOSER B ON A.NUM_IMEI=B.NUM_SERIE AND B.NUM_SERIE <> '999999999999999'" +
						"                                LEFT JOIN ga_cont_arr_copc C ON A.NUM_ABONADO=C.NUM_ABONADO AND A.NUM_IMEI = C.num_serie" +
						"                                LEFT JOIN GA_TIPCONTRATO E ON A.COD_TIPCONTRATO=E.COD_TIPCONTRATO" +
						"                                LEFT JOIN TMCFAC_GE_CARGOS_AGRUPA J ON A.NUM_IMEI = J.NUM_SERIE AND J.COD_CONCEPTO_DTO IN(9217,9218)" +
						"                                LEFT JOIN FA_HISTCARGOS I ON A.COD_CLIENTE=I.COD_CLIENTE AND A.NUM_ABONADO=I.NUM_ABONADO AND I.COD_CONCEPTO IN(9217,9218)" +
						"                                LEFT JOIN GE_CLIENTES F ON A.COD_CLIENTE=F.COD_CLIENTE" +
						"                                LEFT JOIN AL_ARTICULOS G ON B.COD_ARTICULO=G.COD_ARTICULO" +
						"                                LEFT JOIN AL_FABRICANTES H ON G.COD_FABRICANTE=H.COD_FABRICANTE" +
						"                            WHERE" +
						"                                A.NUM_CELULAR = 985027425" +
						"                                ORDER BY NUM_IMEI DESC, COD_SITUACION;";
				System.out.println(queryString);
				ResultSet rs = statement.executeQuery(queryString);
				
		        if(null != rs) {
		        	
		        	int i = 0;
		        	while (rs.next()) {
		        		
		        		if (rs.getString(1).equalsIgnoreCase("") || rs.getString(1).equalsIgnoreCase(null) || rs.getString(1).equalsIgnoreCase("_") ) {	
		        			
		        		}
		        		else {
		        			if (rs.getString("COD_SITUACION").equalsIgnoreCase("AAA") || rs.getString("COD_SITUACION").equalsIgnoreCase("SAA") ) {
		        			
		        				System.out.println("Linea "+rs.getString(1)+" N�mero  "+rs.getString("NUM_CELULAR"));
		        			
		        			}
		        		}
		        		i++;
			         }
		        }
		   }
		   catch (SQLException e) {
				
				System.out.println("Error en la consulta a la base de datos "+e);
			}

	   return costoSalida;
	   }
}
